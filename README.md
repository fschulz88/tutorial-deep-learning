# Machine Learning para Transferência de Estilo Artístico

Esse repositório contém os *Jupyter Notebooks* do tutorial de uso de redes neurais convolucionais para a técnica de **transferência de estilo artístico**.

## Setup

1. Instale o Anaconda: O [**Anaconda**](https://www.anaconda.com/download/) é uma distribuição do Python que já vem com quase tudo que vamos usar instalado previamente.

2. Crie um ambiente virtual Anaconda: Uma vez que o Anaconda estiver instalado, crie um ambiente específico para esse tutorial:

    ````
    conda create -n transf-art-style python=3.6 anaconda
    ````
    
    para criar um ambiente chamado 'transf-art-style'.

    Agora, sempre que quiser trabalhar nesse ambiente, basta digitar:

    ````
    source activate transf-art-style
    ````

3. Uma vez que o ambiente virtual estiver rodando, é bem simples instalar os *notebooks* do tutorial. Primeiro, instale algumas dependências: 

    ````
    pip install tensorflow
    pip install keras
    ````

    Agora clone uma cópia do repositório:

    ````
    git clone https://gitlab.com/fschulz88/tutorial-deep-learning.git
    ````

4. É isso! Agora coloque o Jupyter para rodar e acesse pelo navegador.
    ````
    jupyter notebook
    ````

## Link para formulário de avaliação do tutorial

[Google Forms](https://goo.gl/forms/cxVBo39WbWfvguC53)