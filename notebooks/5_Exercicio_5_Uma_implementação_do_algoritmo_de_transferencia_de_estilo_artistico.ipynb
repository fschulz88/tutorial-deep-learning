{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Uma implementação concreta do algoritmo de transferência de estilo artístico\n",
    "\n",
    "Como sempre, nós começamos importando alguns pacotes. Note que não são tantos."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import print_function\n",
    "\n",
    "import time\n",
    "from PIL import Image\n",
    "import numpy as np\n",
    "\n",
    "from keras import backend\n",
    "from keras.models import Model\n",
    "from keras.applications.vgg16 import VGG16\n",
    "\n",
    "from scipy.optimize import fmin_l_bfgs_b\n",
    "from scipy.misc import imsave"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Carregar e pre-processar as imagens de conteúdo e estilo\n",
    "\n",
    "Nossa primeira tarefa é carregar as imagens de conteúdo e estilo. Note que a imagem de conteúdo que estamos usando não é uma foto particularmente de alta qualidade, mas a saída que vamos obter ainda parece muito boa."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "height = 512\n",
    "width = 512\n",
    "\n",
    "content_image_path = 'images/karl_ove_knausgaard.jpg'\n",
    "content_image = Image.open(content_image_path)\n",
    "content_image = content_image.resize((width, height))\n",
    "content_image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "style_image_path = 'images/wave.jpg'\n",
    "style_image = Image.open(style_image_path)\n",
    "style_image = style_image.resize((width, height))\n",
    "style_image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Agora convertemos essas imagens em uma forma própria para processamento numérico. Em particular, nós acrescentamo uma nova dimensão (além da clássica forma *altura x largura x 3*), para que possamos concatenar, mais à frente, as representações dessas duas imagens em uma estrutura de dados em comum."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "content_array = np.asarray(content_image, dtype='float32')\n",
    "content_array = np.expand_dims(content_array, axis=0)\n",
    "print(content_array.shape)\n",
    "\n",
    "style_array = np.asarray(style_image, dtype='float32')\n",
    "style_array = np.expand_dims(style_array, axis=0)\n",
    "print(style_array.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Antes de avançar, precisamos manipular os dados de entrada para refletir o que foi feito no artigo que introduziu o modelo das redes VGG que vamos usar.\n",
    "\n",
    "Para isso, precisamos realizar duas transformações:\n",
    "\n",
    "1. Subtrair um valor RGB médio de cada *pixel* (computado previamente a partir no conjunto ImageNet)\n",
    "2. Mudar a ordem do vetor RGB multidimensional de RGB para BGR (a ordem usada no artigo)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "content_array[:, :, :, 0] -= 103.939\n",
    "content_array[:, :, :, 1] -= 116.779\n",
    "content_array[:, :, :, 2] -= 123.68\n",
    "content_array = content_array[:, :, :, ::-1]\n",
    "\n",
    "style_array[:, :, :, 0] -= 103.939\n",
    "style_array[:, :, :, 1] -= 116.779\n",
    "style_array[:, :, :, 2] -= 123.68\n",
    "style_array = style_array[:, :, :, ::-1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Agora estamos prontos para usar esses vetores para definir variáveis no *Keras*. Também introduzimos uma variável de preenchimento para guardar a imagem de combinação que registra o conteúdo da imagem de conteúdo e incorpora o estilo da imagem de estilo."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "content_image = backend.variable(content_array)\n",
    "style_image = backend.variable(style_array)\n",
    "combination_image = backend.placeholder((1, height, width, 3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finalmente, concatenamos todos esses dados em um único *tensor* que é próprio para processar o modelo VGG16 do *Keras*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "input_tensor = backend.concatenate([content_image,\n",
    "                                    style_image,\n",
    "                                    combination_image], axis=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reusar um modelo pré-treinado para classificação de imagens para definir funções de perda\n",
    "\n",
    "A ideia-chave introduzida por [Gatys et al.](https://arxiv.org/abs/1508.06576) é que as redes neurais convolucionais pré-treinadas para classificação de imagens já sabem como codificar informações perceptivas e semânticas das imagens. Nós vamos seguir essa ideia e usar o espaço de características providas por esse modelo para trabalhar independentemente com conteúdo e estilo de imagens.\n",
    "\n",
    "O artigo original usa o modelo VGG de 19 camadas de [Simonyan and Zisserman](https://arxiv.org/abs/1409.1556), mas vamos seguir [Johnson et al.](https://arxiv.org/abs/1603.08155) e usar o modelo de 16 camadas. Não há uma diferença qualitativa notável ao fazer essa escolha, e ganhamos um pouco em velocidade.\n",
    "\n",
    "Além disso, como não estamos (mais) interessados no problema da classificação, não precisamos das camadas de redes integralmente conectadas ou do classificador *softmax*. Nós só precisamos da parte do modelo marcada em verde na tabela a seguir.\n",
    "\n",
    "![VGG Network Architectures](images/vgg-architecture.png \"VGG Network Architectures\")\n",
    "\n",
    "É trivial para nós acessar esse modelo truncado porque o *Keras* vem com um conjunto de modelos pré-treinados, incluindo o modelo `VGG16`. Note que, ao passar o código `include_top=False` no código a seguir, nós não incluimos nenhuma das camadas integralmente conectadas."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = VGG16(input_tensor=input_tensor, weights='imagenet',\n",
    "              include_top=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Como fica claro na figura da tabela acima, o modelo com o qual nós estamos trabalhamos tem várias camadas. O Keras tem sua própria nomenclatura para essas camadas. Vamos listar esses nomes para que possamos fazer referência a eles individualmente"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "layers = dict([(layer.name, layer.output) for layer in model.layers])\n",
    "layers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Se você olhar a lista acima, você verá que cobrimos todos os itens que queríamos da tabela da arquitetura do modelo `VGG16`. Note também que, como já alimentamos o *Keras* com um *tensor* de entrada concreto, os vários tensores do *TensorFlow* já tem formas (*shapes*) bem definidos.\n",
    "\n",
    "---\n",
    "\n",
    "A grande ideia que estamos tentando entender e aplicar é que o problema da transferência de estilo pode ser expresso como um problema de otimização, onde a função de perda que queremos minimizar pode ser decomposta em três partes distintas: a *perda de conteúdo*, a *perda de estilo* e a *perda de variação total*.\n",
    "\n",
    "A importância relativa desses termos é determinada por um conjunto de pesos escalares. Eles são arbitrários, mas o conjunto a seguir foi escolhido após bastante experimentação para encontrar um conjunto que gera uma saída que é esteticamente agradável."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "content_weight = 0.025\n",
    "style_weight = 5.0\n",
    "total_variation_weight = 1.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Agora vamos usar os espaços de características fornecidos pelas camadas específicas do nosso modelo para definir essas três funções de perda. Começamos inicializar a perda total como `0` e incrementá-la aos poucos."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "loss = backend.variable(0.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Perda de conteúdo\n",
    "\n",
    "Para a perda de conteúdo, seguimos Johnson et al. e retiramos as características de conteúdo do `block2_conv2`, porque a escolha original de Gatys et al. (`block4_conv2`) perde muito dos detalhes estruturais. Pelo menos para rostos e retratos, consideramos mais esteticamente agradável reter a estrutura da imagem de conteúdo original.\n",
    "\n",
    "A variação ao longo das camadas é mostrada em alguns exemplos na figura abaixo (substituindo a notação `reluX_Y` com a notação `blockX_convY` do *Keras*).\n",
    "\n",
    "![Content feature reconstruction](images/content-feature.png \"Content feature reconstruction\")\n",
    "\n",
    "A perda de conteúdo é a distância Euclidiana (escalada e quadrada) entre as características de representação de conteúdo e as imagens combinadas."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "def content_loss(content, combination):\n",
    "    return backend.sum(backend.square(combination - content))\n",
    "\n",
    "layer_features = layers['block2_conv2']\n",
    "content_image_features = layer_features[0, :, :, :]\n",
    "combination_features = layer_features[2, :, :, :]\n",
    "\n",
    "loss += content_weight * content_loss(content_image_features,\n",
    "                                      combination_features)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Perda de estilo\n",
    "\n",
    "Aqui a coisa começa a ficar complicada.\n",
    "\n",
    "Para a perda de estilo, primeiro nós definimos algo chamado de *matriz Gram*. Os termos dessa matriz são proporcionais às covariâncias de conjuntos correspondentes de características, e assim capturam as informações cujas características tendem a se ativar juntas. Ao capturar apenas essas estatísticas agregadas ao longo da imagem, esses termos se tornam “cegos” ao arranjo dos objetos dentro da imagem. Isso é o que permite a eles capturar informações sobre estilo, independente de conteúdo. Isso não é de forma alguma trivial, e [existem bons artigos que explicam essa ideia](https://arxiv.org/abs/1606.01286).)\n",
    "\n",
    "A matriz Gram pode ser computada eficientemente por meio de remodelar os espaços de características de forma apropriada e obter o produto diádico.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gram_matrix(x):\n",
    "    features = backend.batch_flatten(backend.permute_dimensions(x, (2, 0, 1)))\n",
    "    gram = backend.dot(features, backend.transpose(features))\n",
    "    return gram"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A perda de estilo então é a norma de Frobenius (escalada e quadrada) da diferença entre as matrizes Gram de estilo e da imagem combinada.\n",
    "\n",
    "Novamente, no código a seguir, decidimos usar as características de estilo das camadas definidas em Johnson et al., ao invés de Gatys et al., por acharmos que os resultados finais são mais esteticamente agradáveis. Essas escolhas podem ser alteradas para se obter resultados diversos."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "def style_loss(style, combination):\n",
    "    S = gram_matrix(style)\n",
    "    C = gram_matrix(combination)\n",
    "    channels = 3\n",
    "    size = height * width\n",
    "    return backend.sum(backend.square(S - C)) / (4. * (channels ** 2) * (size ** 2))\n",
    "\n",
    "feature_layers = ['block1_conv2', 'block2_conv2',\n",
    "                  'block3_conv3', 'block4_conv3',\n",
    "                  'block5_conv3']\n",
    "for layer_name in feature_layers:\n",
    "    layer_features = layers[layer_name]\n",
    "    style_features = layer_features[1, :, :, :]\n",
    "    combination_features = layer_features[2, :, :, :]\n",
    "    sl = style_loss(style_features, combination_features)\n",
    "    loss += (style_weight / len(feature_layers)) * sl"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Perda de variação total\n",
    "\n",
    "As coisas voltam a ficar mais simples agora.\n",
    "\n",
    "Se você tentasse resolver o problema da otimização com apenas as duas perdas que já introduzimos (estilo e conteúdo), você obteria um resultado um tanto quanto ruidoso. Por isso, adicionamos mais um termo, chamado de perda de variação total, que leva a uma suavidade espacial.\n",
    "\n",
    "Você pode experimentar reduzir o parâmetro `total_variation_weight` e alterar os níveis de ruído da imagem gerada."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "def total_variation_loss(x):\n",
    "    a = backend.square(x[:, :height-1, :width-1, :] - x[:, 1:, :width-1, :])\n",
    "    b = backend.square(x[:, :height-1, :width-1, :] - x[:, :height-1, 1:, :])\n",
    "    return backend.sum(backend.pow(a + b, 1.25))\n",
    "\n",
    "loss += total_variation_weight * total_variation_loss(combination_image)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Definir os gradientes necessários e resolver o problema de otimização\n",
    "\n",
    "O objetivo de todo esse trabalho era elaborar um problema de otimização cuja solução é uma imagem combinada que contém o conteúdo de uma imagem e o estilo de outra. Agora que já temos nossas imagens de entrada preparadas e nossos cálculos de função de perda no lugar, o que nos resta é definir gadientes para a perda total relativa à imagem combinada, e usar esses gradientes para otimizar iterativamente sobre nossa imagem combinada para minimizar a perda.\n",
    "\n",
    "Nós começamos definindo nossos gradientes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "grads = backend.gradients(loss, combination_image)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Agora introduzimos uma classe `Evaluator`, que calcula perda e gradientes em uma passada, enquanto os recebe por meio de duas funções separadas, `loss` e `grads`. Isso é feito porque o `scipy.optimize` requer funções separadas para perda e gradientes, mas computá-los separadamente não seria eficiente."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "outputs = [loss]\n",
    "outputs += grads\n",
    "f_outputs = backend.function([combination_image], outputs)\n",
    "\n",
    "def eval_loss_and_grads(x):\n",
    "    x = x.reshape((1, height, width, 3))\n",
    "    outs = f_outputs([x])\n",
    "    loss_value = outs[0]\n",
    "    grad_values = outs[1].flatten().astype('float64')\n",
    "    return loss_value, grad_values\n",
    "\n",
    "class Evaluator(object):\n",
    "\n",
    "    def __init__(self):\n",
    "        self.loss_value = None\n",
    "        self.grads_values = None\n",
    "\n",
    "    def loss(self, x):\n",
    "        assert self.loss_value is None\n",
    "        loss_value, grad_values = eval_loss_and_grads(x)\n",
    "        self.loss_value = loss_value\n",
    "        self.grad_values = grad_values\n",
    "        return self.loss_value\n",
    "\n",
    "    def grads(self, x):\n",
    "        assert self.loss_value is not None\n",
    "        grad_values = np.copy(self.grad_values)\n",
    "        self.loss_value = None\n",
    "        self.grad_values = None\n",
    "        return grad_values\n",
    "\n",
    "evaluator = Evaluator()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Agora estamos finalmente prontos para resolver nosso problema de otimização. A imagem combinada começa sua vida como uma coleção aleatória de *pixels* (válidos), e usamos o algoritmo [L-BFGS](https://en.wikipedia.org/wiki/Limited-memory_BFGS) (um algoritmo *quasi*-Newtoniano que é significativamente mais rápido para convergir que o gradiente descendente padrão) para otimizar sobre ela.\n",
    "\n",
    "Paramos após 10 iterações porque o resultado já é satisfatório a esse ponto e a perda deixa de ser significativa daí em diante. Note que nós precisamos passar nossa imagem de saída pelo inverso das transformações que fizemos com a entrada, para que ela possa fazer sentido visual. Cada iteração é salva em um arquivo cujo nome indica os parâmetros de perda de estilo, conteúdo e variação total, e o contador da iteração, para que possamos visualizar cada passo do processo separadamente."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if backend.image_dim_ordering() == 'th':\n",
    "    x = np.random.uniform(0, 255, (1, 3, height, width)) - 128.\n",
    "else:\n",
    "    x = np.random.uniform(0, 255, (1, height, width, 3)) - 128.\n",
    "\n",
    "def deprocess_image(x):\n",
    "    if backend.image_dim_ordering() == 'th':\n",
    "        x = x.reshape((3, height, width))\n",
    "        x = x.transpose((1, 2, 0))\n",
    "    else:\n",
    "        x = x.reshape((height, width, 3))\n",
    "    x = x[:, :, ::-1]\n",
    "    x[:, :, 0] += 103.939\n",
    "    x[:, :, 1] += 116.779\n",
    "    x[:, :, 2] += 123.68\n",
    "    x = np.clip(x, 0, 255).astype('uint8')\n",
    "    return x\n",
    "\n",
    "iterations = 10\n",
    "\n",
    "for i in range(iterations):\n",
    "    print('Inicio da iteracao', i)\n",
    "    start_time = time.time()\n",
    "    x, min_val, info = fmin_l_bfgs_b(evaluator.loss, x.flatten(),\n",
    "                                     fprime=evaluator.grads, maxfun=20)\n",
    "    print('Valor atual de perda:', min_val)\n",
    "\n",
    "    img = deprocess_image(x.copy())\n",
    "    fname = os.path.join('cw_%g_sw_%g_tvw_%g_i_%d.png' %\n",
    "                         (content_weight,\n",
    "                          style_weight,\n",
    "                          total_variation_weight,\n",
    "                          i))\n",
    "    imsave(fname, img)\n",
    "    end_time = time.time()\n",
    "    print('Iteracao %d completa em %ds' % (i, end_time - start_time))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Esse processo leva algum tempo, mesmo em um computador com GPU dedicada, então não se surpreenda se demorar bem mais em um sem.\n",
    "\n",
    "As figuras a seguir apresentam os resultados concatenados das 10 iterações e o resultado final.\n",
    "![Iterations](images/karl-ove-results.png \"A saída das dez iterações.\")\n",
    "![Result](images/karl-ove-final.png \"O resultado final.\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
